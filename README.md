Arch Setup
==========

This is a collection of notes from my arch installation.

1. Install arch, include base-devel, sudo
	- https://wiki.archlinux.org/index.php/Installation_guide
	- fdisk partitioning
		- https://www.youtube.com/watch?v=4PBqpX0_UOc @ 18:00
	- Formatting disks
		- Boot partition
			- mkfs.fat -F32 /path/to/drive
	- Timezone
		- ln -sf /usr/share/zoneinfo/America/Denver /etc/localtime
	- Install network manager once chroot-ed into the install
	- Install systemd -> https://wiki.archlinux.org/index.php/Systemd-bootg
		- bootctl
		- https://www.addictivetips.com/ubuntu-linux-tips/set-up-systemd-boot-on-arch-linux/
		- blkid -s PARTUUID -o value /dev/sdxY
2. Add a new user w/password
	- https://wiki.archlinux.org/index.php/Users_and_groups#User_management
	- Add this user to the wheelgroup
	- Enable wheel to use sudo
		- EDITOR=nano
		- visudo -> remove # from infront of %wheel
3. Install xorg xorg-server gnome gnome-extra networkmanager network-manager-applet gnome-keyring
	- Enable NetworkManager
	- Enable gdm
4. Install yay-bin -> https://arcolinuxd.com/get-your-aur-helpers-in-on-any-arch-linux-based-system/
5. Install pamac -> yay -S pamac-aur
6. Install ansible
7. Run ansible scripts


## Setting up on proxmox/virtualbox
https://www.howtoforge.com/tutorial/install-arch-linux-on-virtualbox/


## Notes for MacBook
https://medium.com/@philpl/arch-linux-running-on-my-macbook-2ea525ebefe3

## If you want grub
- Install grub -> https://wiki.archlinux.org/index.php/GRUB
	- grub efibootmgr
	- grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB	