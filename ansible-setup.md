Ansible Setup
=============

1. Install ansible
2. Install ansible-aur-git -> https://github.com/kewlfft/ansible-aur
3. Run ansible -> ansible-playbook playbook.yaml --ask-become-pass

## Local directives
https://www.tricksofthetrades.net/2017/10/02/ansible-local-playbooks/